package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtNombre;
    private Button btnEntrar;
    private Button btnSalir;
    private Rectangulo rectangulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre = (EditText) findViewById(R.id.txtNombre);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnSalir = (Button) findViewById(R.id.btnSalir);

        rectangulo = new Rectangulo();

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();
                if(!nombre.matches("")) {
                    Intent intent = new Intent(MainActivity.this, RectanguloActivity.class);
                    intent.putExtra("nombre",nombre);
                    Bundle object = new Bundle();
                    object.putSerializable("rectangulo",rectangulo);
                    intent.putExtras(object);
                    startActivity(intent);

                } else {
                    Toast.makeText(MainActivity.this, "Escriba su nombre", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
