package com.example.examen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class RectanguloActivity extends AppCompatActivity {

    private TextView lblNombre;
    private EditText txtBase;
    private EditText txtAltura;
    private TextView lblArea;
    private TextView lblPerimetro;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private Rectangulo rectangulo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rectangulo_activity);

        lblNombre= (TextView) findViewById(R.id.lblNombreR);
        lblPerimetro = (TextView) findViewById(R.id.lblPerimetro);
        lblArea= (TextView) findViewById(R.id.lblArea);
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        txtBase = (EditText) findViewById(R.id.txtBase);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);

        Bundle datos = getIntent().getExtras();
        lblNombre.setText(datos.getString("nombre"));
        rectangulo = (Rectangulo) datos.getSerializable("rectangulo");

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtAltura.getText().toString().matches("")  || txtBase.getText().toString().matches("")) {
                    Toast.makeText(RectanguloActivity.this, "Ingrese todos los datos", Toast.LENGTH_SHORT).show();
                } else {
                    rectangulo.setAltura(Double.valueOf(txtAltura.getText().toString()));
                    rectangulo.setBase(Double.valueOf(txtBase.getText().toString()));

                    lblArea.setText(String.valueOf(rectangulo.calcularArea()));
                    lblPerimetro.setText(String.valueOf(rectangulo.calcularPerimetro()));
                }

            }
        });


        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblArea.setText("");
                lblPerimetro.setText("");
                txtBase.setText("");
                txtAltura.setText("");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
